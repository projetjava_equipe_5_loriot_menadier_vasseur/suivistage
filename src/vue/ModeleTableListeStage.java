/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import modele.metier.LesStages;

/**
 *
 * @author Freazar
 */
public class ModeleTableListeStage extends AbstractTableModel {

    private String[] lesNomsDeColonnes = {"IdStage", "idEtudiant","IdProfesseurVisite", "IdProfesseurSuivi", "Classe", "Nom", "Prenom", "ProfesseurVisite", "ProfesseurSuivi", "Annee"};
    private ArrayList<LesStages> lesStages;
    private final int NB_COLONNES = 10;


    public ModeleTableListeStage(ArrayList<LesStages> desStages) {
        super();
        lesStages = desStages;
    }

    public ModeleTableListeStage() {
        super();
        lesStages = new ArrayList<>();
    }

    @Override
    public String getColumnName(int column) {
        return lesNomsDeColonnes[column];
    }

    public void addRow(LesStages unStage) {
        lesStages.add(unStage);
    }

    @Override
    public int getRowCount() {
        return lesStages.size();
    }

    public void clear() {
        lesStages.clear();
    }

    @Override
    public int getColumnCount() {
        return NB_COLONNES;
    }

    public LesStages getLesStages(int numLigne) {
        return lesStages.get(numLigne);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value;
        LesStages desStages = getLesStages(rowIndex);
        switch (columnIndex) {
            case 0: {
                value = desStages.getIdStage();
                break;
            }
            case 1: {
                value = desStages.getIdEtudiant();
                break;
            }
            case 2: {
                value = desStages.getIdProfesseurVisite();
                break;
            }
            case 3: {
                value = desStages.getIdProfesseurSuivi();
                break;
            }
            case 4: {
                value = desStages.getClasse();
                break;
            }
            case 5: {
                value = desStages.getNom();
                break;
            }
            case 6: {
                value = desStages.getPrenom();
                break;
            }
            case 7: {
                value = desStages.getProfesseurVisite();
                break;
            }
            case 8: {
                value = desStages.getProfesseurSuivi();
                break;
            }
            case 9: {
                value = desStages.getPromotion();
                break;
            }
            default: {
                value = null;
                break;
            }
        }
        return value;
    }

}
