/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import modele.metier.LeStage;
/**
 *
 * @author Freazar
 */
public class ModeleTableDetailStage  extends AbstractTableModel {


    private String[] lesNomsDeColonnes = {"idStage", "idEntreprise", "idMaitreStage", "idEtudiant", "dateDebut", "dateFin", "isDone"};
    private ArrayList<LeStage> leStage;
    private final int NB_COLONNES = 7;

    public ModeleTableDetailStage(ArrayList<LeStage> desStages) {
        super();
        leStage = desStages;
    }

    public ModeleTableDetailStage() {
        super();
        leStage = new ArrayList<>();
    }

    @Override
    public String getColumnName(int column) {
        return lesNomsDeColonnes[column];
    }

    public void addRow(LeStage unStage) {
        leStage.add(unStage);
    }

    @Override
    public int getRowCount() {
        return leStage.size();
    }

    public void clear() {
        leStage.clear();
    }

    @Override
    public int getColumnCount() {
        return NB_COLONNES;
    }

    public LeStage getStage(int numLigne) {
        return leStage.get(numLigne);
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        return null;
    }

}

