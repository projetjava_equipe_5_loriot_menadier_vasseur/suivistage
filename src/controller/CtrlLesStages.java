/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.dao.DaoLesStages;
import modele.metier.LesStages;
import vue.LesSuiviStages;
import vue.ModeleTableListeStage;

/**
 *
 * @author Freazar
 */
public class CtrlLesStages implements ActionListener, WindowListener, MouseListener{

    private LesSuiviStages vue;         // LA VUE
    private CtrlPrincipal ctrlPrincipal;
    private List<LesStages> lesStages;        // La liste des stages à afficher

    public CtrlLesStages(LesSuiviStages vue, CtrlPrincipal ctrl) {
        this.vue = vue;
        this.ctrlPrincipal = ctrl;
        this.vue.setControllerLesStages(this);
        // le contrôleur écoute la vue
        this.vue.addWindowListener(this);
        this.vue.getjTableLesStages().addMouseListener(this);
    }
    
    public void afficherLesStages(String nom) throws SQLException {
        // Array des stages init à vide ou à plein
        List<LesStages> InitLesStages = new ArrayList<LesStages>();
        if (nom != null) {
            // DO something
            ((ModeleTableListeStage)vue.getModeleTableListeStage()).clear();
            try {
                InitLesStages = DaoLesStages.selectStagesNomEtudiant(nom);     
                for (LesStages unStage : InitLesStages) {
                    ((ModeleTableListeStage)vue.getModeleTableListeStage()).addRow(unStage);
                }
            } catch (SQLException e) {
                System.out.println("Erreur : " + e);
            }
        } else {
            // A remplir si l'array est vide de base, sinon on filtre en haut
            // Affichage de la liste des stages
            ((ModeleTableListeStage)vue.getModeleTableListeStage()).clear();
            InitLesStages = DaoLesStages.selectAll();
            for (LesStages unStage : InitLesStages) {
                ((ModeleTableListeStage)vue.getModeleTableListeStage()).addRow(unStage);
            }
        }
       ((ModeleTableListeStage) this.vue.getModeleTableListeStage()).fireTableDataChanged();
    }
    
    public void afficherLesStages() throws SQLException {
        this.afficherLesStages(null);
    }
    
    public LesSuiviStages getVue() {
        return vue;
    }

    public void setVue(LesSuiviStages vue) {
        this.vue = vue;
    }

    public CtrlPrincipal getCtrlPrincipal() {
        return ctrlPrincipal;
    }

    public void setCtrlPrincipal(CtrlPrincipal ctrlPrincipal) {
        this.ctrlPrincipal = ctrlPrincipal;
    }

    public List<LesStages> getLesStages() {
        return lesStages;
    }

    public void setLesStages(List<LesStages> lesStages) {
        this.lesStages = lesStages;
    }
    

    @Override
    public void actionPerformed(ActionEvent arg0) {
       
    }

    @Override
    public void windowOpened(WindowEvent arg0) {
    }

    @Override
    public void windowClosing(WindowEvent arg0) {
    }

    @Override
    public void windowClosed(WindowEvent arg0) {
    }

    @Override
    public void windowIconified(WindowEvent arg0) {
    }

    @Override
    public void windowDeiconified(WindowEvent arg0) {
    }

    @Override
    public void windowActivated(WindowEvent arg0) {
    }

    @Override
    public void windowDeactivated(WindowEvent arg0) {
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
    }
    
}
