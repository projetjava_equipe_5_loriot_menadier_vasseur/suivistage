
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modele.metier.LesStages;
import modele.metier.Stage;
        
/**
 *
 * @author Freazar
 */
public class CtrlPrincipal {
    CtrlLeStage ctrlLeStage = null;
    CtrlLesStages ctrlLesStages = null;
    
    public CtrlPrincipal() {
        
    }
    
        /**
     * Proposer à l'utilisateur de mettre fin au programme
     * @param vue vue à l'origine de la demande
     */
    public void quitterApplication(JFrame vue) {
        // Confirmer avant de quitter
        int rep = JOptionPane.showConfirmDialog(vue, "Quitter l'application\nEtes-vous sûr(e) ?",
                "STAGE", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (rep == JOptionPane.YES_OPTION) {
            // mettre fin à l'application
            System.exit(0);
        }
    }
    
    /**
     * Afficher la vue "Suivi Stage"
     * @param unStage stage à afficher
     */
    public void afficherUnStage(Stage unStage) {
        ctrlLeStage.setLeStage(unStage);
        ctrlLeStage.afficherLeStage();
        ctrlLeStage.getVue().setVisible(true);
        // Les autres controlleurs sont invisibles
        ctrlLesStages.getVue().setEnabled(false);
    }
    
    /**
     * Afficher la vue "Suivi de tous les stages"
     * @param desStages liste des stages à afficher
     */
    public void afficherLesStages(List<LesStages> desStages) throws SQLException{
        ctrlLesStages.setLesStages(desStages);
        ctrlLesStages.afficherLesStages();
        ctrlLesStages.getVue().setVisible(true);
        ctrlLesStages.getVue().setEnabled(true);
        ctrlLesStages.getVue().getjTableLesStages().setEnabled(false);
//        if (ctrlLesStages != null) {
//            ctrlLeStage.getVue().setVisible(false);    // fenêtre modale
//        }
    }
    
     /**
     * De retour de la liste
     * la fenêtre fiche est masquée, la fenêtre liste redevient active
     */
    public void retourLesStages() {
        ctrlLeStage.getVue().setVisible(false);
        ctrlLesStages.getVue().setEnabled(true);
    }
    
    // Accesseurs et mutateurs

    public CtrlLeStage getCtrlLeStage() {
        return ctrlLeStage;
    }

    public void setCtrlLeStage(CtrlLeStage ctrlLeStage) {
        this.ctrlLeStage = ctrlLeStage;
    }

    public CtrlLesStages getCtrlLesStages() {
        return ctrlLesStages;
    }

    public void setCtrlLesStages(CtrlLesStages ctrlLesStages) {
        this.ctrlLesStages = ctrlLesStages;
    }
    
    
}
