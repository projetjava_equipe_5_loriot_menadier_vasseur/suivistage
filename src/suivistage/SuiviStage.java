/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suivistage;

import controller.CtrlLesStages;
import controller.CtrlPrincipal;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modele.dao.DaoLesStages;
import modele.dao.Jdbc;
import vue.LesSuiviStages;

/**
 *
 * @author melo_
 */
public class SuiviStage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException {
        
        CtrlPrincipal leControleurPrincipal ;
        LesSuiviStages vueLesSuiviStages;
        CtrlLesStages leControleurSuiviStages;
        
        
        
        Jdbc.creerDefault();
        try {
            Jdbc.getInstance().connecter();

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Main - échec de connexion"+ ex);
        }
        
        leControleurPrincipal = new CtrlPrincipal();
        vueLesSuiviStages = new LesSuiviStages();
        leControleurSuiviStages = new CtrlLesStages(vueLesSuiviStages, leControleurPrincipal);  
        leControleurPrincipal.setCtrlLesStages(leControleurSuiviStages);
        leControleurPrincipal.afficherLesStages(DaoLesStages.selectAll());
        
    }
    
}
