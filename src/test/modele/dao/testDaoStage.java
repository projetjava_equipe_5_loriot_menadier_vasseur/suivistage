/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.modele.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import modele.dao.DaoStage;
import modele.dao.Jdbc;
import modele.metier.Stage;

/**
 *
 * @author Freazar
 */
public class testDaoStage {

    /**
    * @param args the command line arguments
    */
    public static void main(String[] args) {

        java.sql.Connection cnx = null;

        try {
            test0_Connexion();
            System.out.println("Test0 effectué : connexion\n");
            test1_SelectUnique(1);
            System.out.println("Test1 effectué : sélection unique\n");
            test2_SelectMultiple();
            System.out.println("Test2 effectué : sélection multiple\n");
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD
     * @throws SQLException
     */
    public static void test0_Connexion() throws  SQLException {
        Jdbc.creer("jdbc:mysql://localhost/", "SUIVISTAGE", "root", "");
//        Jdbc.creer("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:", "@localhost:1521:XE", "", "btssio", "btssio");
        Jdbc.getInstance().connecter();
        Connection cnx = Jdbc.getInstance().getConnexion();
    }

    /**
     * Affiche un stage d'après son identifiant
     * @param idStage
     * @throws SQLException
     */
    public static void test1_SelectUnique(int idStage) throws SQLException {
        Stage ceStage = DaoStage.selectOne(idStage);
        System.out.println("Stage d'identifiant : " + idStage + " : " + ceStage.toString());
    }

    /**
     * Affiche tous les stages
     * @throws SQLException
     */
    public static void test2_SelectMultiple() throws SQLException {
        List<Stage> desStages = DaoStage.selectAll();
        System.out.println("Les stages lus : " + desStages.toString());
    }
    
}
