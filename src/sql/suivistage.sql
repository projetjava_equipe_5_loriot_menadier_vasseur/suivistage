-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : sam. 10 avr. 2021 à 13:06
-- Version du serveur :  8.0.21
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `suivistage`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

DROP TABLE IF EXISTS `administrateur`;
CREATE TABLE IF NOT EXISTS `administrateur` (
  `IDADMINISTRATEUR` int NOT NULL,
  `PRENOM` varchar(32) DEFAULT NULL,
  `NOM` varchar(32) DEFAULT NULL,
  `LOGIN` varchar(32) DEFAULT NULL,
  `MDP` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`IDADMINISTRATEUR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `anneescolaire`
--

DROP TABLE IF EXISTS `anneescolaire`;
CREATE TABLE IF NOT EXISTS `anneescolaire` (
  `PROMOTION` varchar(32) NOT NULL,
  PRIMARY KEY (`PROMOTION`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `anneescolaire`
--

INSERT INTO `anneescolaire` (`PROMOTION`) VALUES
('2020/2021');

-- --------------------------------------------------------

--
-- Structure de la table `anneesuivie`
--

DROP TABLE IF EXISTS `anneesuivie`;
CREATE TABLE IF NOT EXISTS `anneesuivie` (
  `IDETUDIANT` int NOT NULL,
  `PROMOTION` varchar(32) NOT NULL,
  `IDCLASSE` int NOT NULL,
  PRIMARY KEY (`IDETUDIANT`,`PROMOTION`),
  KEY `FK_ANNEESUIVIE_CLASSE` (`IDCLASSE`),
  KEY `FK_ANNEESUIVIE_ANNEESCOLAIRE` (`PROMOTION`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `anneesuivie`
--

INSERT INTO `anneesuivie` (`IDETUDIANT`, `PROMOTION`, `IDCLASSE`) VALUES
(1, '2020/2021', 2),
(2, '2020/2021', 1),
(3, '2020/2021', 2),
(4, '2020/2021', 4),
(5, '2020/2021', 3),
(6, '2020/2021', 3);

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

DROP TABLE IF EXISTS `classe`;
CREATE TABLE IF NOT EXISTS `classe` (
  `IDCLASSE` int NOT NULL,
  `LIBELLECLASSE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`IDCLASSE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`IDCLASSE`, `LIBELLECLASSE`) VALUES
(4, '2SISR'),
(3, '2SLAM'),
(2, '1SIOB'),
(1, '1SIOA');

-- --------------------------------------------------------

--
-- Structure de la table `enseignement`
--

DROP TABLE IF EXISTS `enseignement`;
CREATE TABLE IF NOT EXISTS `enseignement` (
  `IDPROFESSEUR` int NOT NULL,
  `IDCLASSE` int NOT NULL,
  `PROMOTION` varchar(32) NOT NULL,
  PRIMARY KEY (`IDPROFESSEUR`,`IDCLASSE`,`PROMOTION`),
  KEY `FK_ENSEIGNEMENT_CLASSE` (`IDCLASSE`),
  KEY `FK_ENSEIGNEMENT_ANNEESCOLAIRE` (`PROMOTION`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
CREATE TABLE IF NOT EXISTS `entreprise` (
  `IDENTREPRISE` int NOT NULL,
  `NOM` varchar(32) DEFAULT NULL,
  `ADRESSE` varchar(32) DEFAULT NULL,
  `VILLE` varchar(32) DEFAULT NULL,
  `CODEPOSTAL` varchar(5) DEFAULT NULL,
  `SECTEURACT` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`IDENTREPRISE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`IDENTREPRISE`, `NOM`, `ADRESSE`, `VILLE`, `CODEPOSTAL`, `SECTEURACT`) VALUES
(4, 'Normal', 'Route 200', 'Doublonville', '44100', 'Combat Pokemon'),
(3, 'Insecte', 'Route 150', 'Ecorcia', '44800', 'Combat Pokemon'),
(2, 'Vol', 'Route 120', 'Mauville', '44600', 'Combat Pokemon'),
(1, 'Ligue Pokemon', 'Route victoire', 'Indigo', '44500', 'Combat Pokemon'),
(5, 'Spectre', 'Route 101', 'Rosalia', '44900', 'Combat Pokemon');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
CREATE TABLE IF NOT EXISTS `etudiant` (
  `IDETUDIANT` int NOT NULL,
  `SPECIALITE` varchar(32) DEFAULT NULL,
  `PRENOM` varchar(32) DEFAULT NULL,
  `NOM` varchar(32) DEFAULT NULL,
  `LOGIN` varchar(32) DEFAULT NULL,
  `MDP` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`IDETUDIANT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`IDETUDIANT`, `SPECIALITE`, `PRENOM`, `NOM`, `LOGIN`, `MDP`) VALUES
(4, 'SISR', 'Aurore', 'Diamant', 'adiamant', 'adiamant'),
(3, 'SLAM', 'Pierre', 'Brock', 'pbrock', 'pbrock'),
(2, 'SISR', 'Ondine', 'Misty', 'omisty', 'omisty'),
(1, 'SLAM', 'Sacha', 'Ketchum', 'sketchum', 'sketchum'),
(5, 'SLAM', 'Flora', 'Ruby', 'fruby', 'fruby'),
(6, 'SLAM', 'Barbara', 'Alola', 'balola', 'balola');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `lesstages`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `lesstages`;
CREATE TABLE IF NOT EXISTS `lesstages` (
`CLASSE` varchar(32)
,`IDETUDIANT` int
,`IDPROFESSEURSUIVI` int
,`IDPROFESSEURVISITE` int
,`IDSTAGE` int
,`NOMETUDIANT` varchar(32)
,`PRENOMETUDIANT` varchar(32)
,`PROFESSEURSUIVI` varchar(32)
,`PROFESSEURVISITE` varchar(32)
,`PROMOTION` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `lestage`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `lestage`;
CREATE TABLE IF NOT EXISTS `lestage` (
`ADRESSE` varchar(32)
,`CLASSE` varchar(32)
,`DATEDEB` varchar(32)
,`DATEFIN` varchar(32)
,`ENTREPRISE` varchar(32)
,`IDSTAGE` int
,`MAITREDESTAGE` varchar(32)
,`NOMETUDIANT` varchar(32)
,`PRENOMETUDIANT` varchar(32)
,`PROFESSEURSUIVI` varchar(32)
,`PROFESSEURVISITE` varchar(32)
);

-- --------------------------------------------------------

--
-- Structure de la table `maitrestage`
--

DROP TABLE IF EXISTS `maitrestage`;
CREATE TABLE IF NOT EXISTS `maitrestage` (
  `IDMAITRESTAGE` int NOT NULL,
  `IDENTREPRISE` int NOT NULL,
  `NOM` varchar(32) DEFAULT NULL,
  `PRENOM` varchar(32) DEFAULT NULL,
  `NUMTEL` varchar(32) DEFAULT NULL,
  `MAIL` varchar(32) DEFAULT NULL,
  `FONCTION` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`IDMAITRESTAGE`),
  KEY `FK_MAITRESTAGE_ENTREPRISE` (`IDENTREPRISE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `maitrestage`
--

INSERT INTO `maitrestage` (`IDMAITRESTAGE`, `IDENTREPRISE`, `NOM`, `PRENOM`, `NUMTEL`, `MAIL`, `FONCTION`) VALUES
(5, 5, 'Mortimer', 'Mortimer', '0266597485', 'mmortimer@pokemon.fr', 'Champion'),
(4, 4, 'Blanche', 'Blanche', '0266597485', 'bblanche@pokemon.fr', 'Champion'),
(3, 3, 'Hector', 'Hector', '0266597485', 'hhector@pokemon.fr', 'Champion'),
(2, 2, 'Albert', 'Albert', '0266597485', 'aalbert@pokemon.fr', 'Champion'),
(1, 1, 'Chen', 'Blue', '0266597485', 'bchen@pokemon.fr', 'Maitre de la Ligue');

-- --------------------------------------------------------

--
-- Structure de la table `professeur`
--

DROP TABLE IF EXISTS `professeur`;
CREATE TABLE IF NOT EXISTS `professeur` (
  `IDPROFESSEUR` int NOT NULL,
  `PRENOM` varchar(32) DEFAULT NULL,
  `NOM` varchar(32) DEFAULT NULL,
  `LOGIN` varchar(32) DEFAULT NULL,
  `MDP` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`IDPROFESSEUR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `professeur`
--

INSERT INTO `professeur` (`IDPROFESSEUR`, `PRENOM`, `NOM`, `LOGIN`, `MDP`) VALUES
(3, 'Jessie', 'Team', 'jteam', 'jteam'),
(2, 'James', 'Rocket', 'jrocket', 'jrocket'),
(1, 'Professeur', 'Chen', 'pchen', 'pchen'),
(4, 'Giovanni', 'Boss', 'gboss', 'gboss');

-- --------------------------------------------------------

--
-- Structure de la table `professeur_principal`
--

DROP TABLE IF EXISTS `professeur_principal`;
CREATE TABLE IF NOT EXISTS `professeur_principal` (
  `IDCLASSE` int NOT NULL,
  `PROMOTION` varchar(32) NOT NULL,
  `IDPROFESSEURPRINCIPAL` int NOT NULL,
  PRIMARY KEY (`IDCLASSE`,`PROMOTION`),
  KEY `FK_PROFESSEUR_PRINCIPAL_ANNEESCOLAIRE` (`PROMOTION`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `professeur_principal`
--

INSERT INTO `professeur_principal` (`IDCLASSE`, `PROMOTION`, `IDPROFESSEURPRINCIPAL`) VALUES
(1, '2020/2021', 1),
(2, '2020/2021', 2),
(3, '2020/2021', 3),
(4, '2020/2021', 4);

-- --------------------------------------------------------

--
-- Structure de la table `stage`
--

DROP TABLE IF EXISTS `stage`;
CREATE TABLE IF NOT EXISTS `stage` (
  `IDSTAGE` int NOT NULL,
  `IDENTREPRISE` int NOT NULL,
  `IDMAITRESTAGE` int NOT NULL,
  `IDETUDIANT` int NOT NULL,
  `DATEDEBUT` varchar(32) DEFAULT NULL,
  `DATEFIN` varchar(32) DEFAULT NULL,
  `ISDONE` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`IDSTAGE`),
  KEY `FK_STAGE_ETUDIANT` (`IDETUDIANT`),
  KEY `FK_STAGE_ENTREPRISE` (`IDENTREPRISE`),
  KEY `FK_STAGE_MAITRESTAGE` (`IDMAITRESTAGE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `stage`
--

INSERT INTO `stage` (`IDSTAGE`, `IDENTREPRISE`, `IDMAITRESTAGE`, `IDETUDIANT`, `DATEDEBUT`, `DATEFIN`, `ISDONE`) VALUES
(6, 5, 5, 6, '23/05/2021', '23/06/2021', 0),
(5, 5, 5, 5, '23/05/2021', '23/06/2021', 0),
(4, 4, 4, 4, '23/03/2021', '23/05/2021', 1),
(3, 3, 3, 3, '23/03/2021', '23/05/2021', 1),
(2, 2, 2, 2, '23/03/2021', '23/05/2021', 1),
(1, 1, 1, 1, '23/03/2021', '23/05/2021', 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `IDUTILISATEUR` int NOT NULL,
  `PRENOM` varchar(32) DEFAULT NULL,
  `NOM` varchar(32) DEFAULT NULL,
  `LOGIN` varchar(32) DEFAULT NULL,
  `MDP` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`IDUTILISATEUR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`IDUTILISATEUR`, `PRENOM`, `NOM`, `LOGIN`, `MDP`) VALUES
(1, 'invite1', 'invite1', 'invite1', 'invite2');

-- --------------------------------------------------------

--
-- Structure de la table `visitestage`
--

DROP TABLE IF EXISTS `visitestage`;
CREATE TABLE IF NOT EXISTS `visitestage` (
  `IDVISITESTAGE` int NOT NULL,
  `IDSTAGE` int NOT NULL,
  `IDPROFESSEUR` int NOT NULL,
  `DATE` date DEFAULT NULL,
  `ISVISITED` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`IDVISITESTAGE`),
  KEY `FK_VISITESTAGE_STAGE` (`IDSTAGE`),
  KEY `FK_VISITESTAGE_PROFESSEUR` (`IDPROFESSEUR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `visitestage`
--

INSERT INTO `visitestage` (`IDVISITESTAGE`, `IDSTAGE`, `IDPROFESSEUR`, `DATE`, `ISVISITED`) VALUES
(1, 1, 1, '0000-00-00', 0),
(2, 2, 2, '0000-00-00', 1),
(3, 3, 3, '0000-00-00', 1),
(4, 4, 4, '0000-00-00', 0),
(5, 5, 4, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Structure de la vue `lesstages`
--
DROP TABLE IF EXISTS `lesstages`;

DROP VIEW IF EXISTS `lesstages`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lesstages` (`IDSTAGE`, `IDETUDIANT`, `IDPROFESSEURVISITE`, `IDPROFESSEURSUIVI`, `CLASSE`, `NOMETUDIANT`, `PRENOMETUDIANT`, `PROFESSEURSUIVI`, `PROFESSEURVISITE`, `PROMOTION`) AS   select `s`.`IDSTAGE` AS `IDSTAGE`,`etu`.`IDETUDIANT` AS `IDETUDIANT`,`pvs`.`IDPROFESSEUR` AS `IDPROFESSEUR`,`pp`.`IDPROFESSEURPRINCIPAL` AS `IDPROFESSEURPRINCIPAL`,`c`.`LIBELLECLASSE` AS `LIBELLECLASSE`,`etu`.`NOM` AS `NOM`,`etu`.`PRENOM` AS `PRENOM`,`pvs`.`NOM` AS `NOM`,`p`.`NOM` AS `NOM`,`ans`.`PROMOTION` AS `PROMOTION` from (((((((`etudiant` `etu` join `anneesuivie` `ans` on((`ans`.`IDETUDIANT` = `etu`.`IDETUDIANT`))) join `classe` `c` on((`c`.`IDCLASSE` = `ans`.`IDCLASSE`))) join `professeur_principal` `pp` on((`pp`.`IDCLASSE` = `c`.`IDCLASSE`))) join `professeur` `p` on((`p`.`IDPROFESSEUR` = `pp`.`IDPROFESSEURPRINCIPAL`))) join `stage` `s` on((`s`.`IDETUDIANT` = `etu`.`IDETUDIANT`))) join `visitestage` `vs` on((`vs`.`IDSTAGE` = `s`.`IDSTAGE`))) join `professeur` `pvs` on((`pvs`.`IDPROFESSEUR` = `vs`.`IDPROFESSEUR`)))  ;

-- --------------------------------------------------------

--
-- Structure de la vue `lestage`
--
DROP TABLE IF EXISTS `lestage`;

DROP VIEW IF EXISTS `lestage`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lestage` (`IDSTAGE`, `CLASSE`, `NOMETUDIANT`, `PRENOMETUDIANT`, `PROFESSEURVISITE`, `PROFESSEURSUIVI`, `DATEDEB`, `DATEFIN`, `ENTREPRISE`, `ADRESSE`, `MAITREDESTAGE`) AS   select `s`.`IDSTAGE` AS `IDSTAGE`,`c`.`LIBELLECLASSE` AS `LIBELLECLASSE`,`etu`.`NOM` AS `NOM`,`etu`.`PRENOM` AS `PRENOM`,`p`.`NOM` AS `NOM`,`p`.`NOM` AS `NOM`,`s`.`DATEDEBUT` AS `DATEDEBUT`,`s`.`DATEFIN` AS `DATEFIN`,`e`.`NOM` AS `NOM`,`e`.`ADRESSE` AS `ADRESSE`,`ms`.`NOM` AS `NOM` from ((((((((`etudiant` `etu` join `anneesuivie` `ans` on((`ans`.`IDETUDIANT` = `etu`.`IDETUDIANT`))) join `classe` `c` on((`c`.`IDCLASSE` = `ans`.`IDCLASSE`))) join `stage` `s` on((`s`.`IDETUDIANT` = `etu`.`IDETUDIANT`))) join `visitestage` `vs` on((`vs`.`IDSTAGE` = `s`.`IDSTAGE`))) join `professeur_principal` `pp` on((`pp`.`IDCLASSE` = `c`.`IDCLASSE`))) join `professeur` `p` on((`p`.`IDPROFESSEUR` = `pp`.`IDPROFESSEURPRINCIPAL`))) join `entreprise` `e` on((`e`.`IDENTREPRISE` = `s`.`IDENTREPRISE`))) join `maitrestage` `ms` on((`e`.`IDENTREPRISE` = `e`.`IDENTREPRISE`))) where (`s`.`IDSTAGE` = 1)  ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
