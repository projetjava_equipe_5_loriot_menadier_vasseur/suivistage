/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author Freazar
 */
public class Stage {
    
    private int idStage;
    private int idEntreprise;
    private int idMaitreStage;
    private int idEtudiant;
    private String dateDebut;
    private String dateFin;
    private boolean isDone;

    public Stage(int idStage, int idEntreprise, int idMaitreStage, int idEtudiant, String dateDebut, String dateFin, boolean isDone) {
        this.idStage = idStage;
        this.idEntreprise = idEntreprise;
        this.idMaitreStage = idMaitreStage;
        this.idEtudiant = idEtudiant;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.isDone = isDone;
    }

    public int getIdStage() {
        return idStage;
    }

    public int getIdEntreprise() {
        return idEntreprise;
    }

    public int getIdMaitreStage() {
        return idMaitreStage;
    }

    public int getIdEtudiant() {
        return idEtudiant;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public boolean isIsDone() {
        return isDone;
    }

    public void setIdStage(int idStage) {
        this.idStage = idStage;
    }

    public void setIdEntreprise(int idEntreprise) {
        this.idEntreprise = idEntreprise;
    }

    public void setIdMaitreStage(int idMaitreStage) {
        this.idMaitreStage = idMaitreStage;
    }

    public void setIdEtudiant(int idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    @Override
    public String toString() {
        return "Stage{" + "idStage=" + idStage + ", idEntreprise=" + idEntreprise + ", idMaitreStage=" + idMaitreStage + ", idEtudiant=" + idEtudiant + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", isDone=" + isDone + '}';
    }
    
    
}
