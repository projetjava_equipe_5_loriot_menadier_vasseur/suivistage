/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author Freazar
 */
public class Enseignement {
    private int idProfesseur;
    private int idClasse;
    private String promotion;

    public Enseignement(int idProfesseur, int idClasse, String promotion) {
        this.idProfesseur = idProfesseur;
        this.idClasse = idClasse;
        this.promotion = promotion;
    }

    public int getIdProfesseur() {
        return idProfesseur;
    }

    public void setIdProfesseur(int idProfesseur) {
        this.idProfesseur = idProfesseur;
    }

    public int getIdClasse() {
        return idClasse;
    }

    public void setIdClasse(int idClasse) {
        this.idClasse = idClasse;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }
    
    
}
