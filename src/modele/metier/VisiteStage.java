/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author Freazar
 */
public class VisiteStage {
    
    private int idVisiteStage;
    private int idStage;
    private int idProfesseur;
    private String dateVisite;
    private boolean isVisited;

    public VisiteStage(int idVisiteStage, int idStage, int idProfesseur, String dateVisite, boolean isVisited) {
        this.idVisiteStage = idVisiteStage;
        this.idStage = idStage;
        this.idProfesseur = idProfesseur;
        this.dateVisite = dateVisite;
        this.isVisited = isVisited;
    }

    public int getIdProfesseur() {
        return idProfesseur;
    }

    public void setIdProfesseur(int idProfesseur) {
        this.idProfesseur = idProfesseur;
    }

    public int getIdVisiteStage() {
        return idVisiteStage;
    }

    public int getIdStage() {
        return idStage;
    }

    public String getDateVisite() {
        return dateVisite;
    }

    public boolean isIsVisited() {
        return isVisited;
    }

    public void setIdVisiteStage(int idVisiteStage) {
        this.idVisiteStage = idVisiteStage;
    }

    public void setIdStage(int idStage) {
        this.idStage = idStage;
    }

    public void setDateVisite(String dateVisite) {
        this.dateVisite = dateVisite;
    }

    public void setIsVisited(boolean isVisited) {
        this.isVisited = isVisited;
    }
        

}
