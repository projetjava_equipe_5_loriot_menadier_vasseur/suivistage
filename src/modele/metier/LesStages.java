/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author dami2
 */
public class LesStages {
    
    private int idStage;
    private int idEtudiant;
    private int idProfesseurVisite;
    private int idProfesseurSuivi;
    private String classe;
    //nom et prenom de l'élève
    private String nom;
    private String prenom;
    
    private String professeurVisite;
    private String professeurSuivi;
    private String promotion;

    public LesStages(int idStage, int idEtudiant, int idProfesseurVisite, int idProfesseurSuivi, String classe, 
            String nom, String prenom, String professeurVisite, String professeurSuivi, String promotion) {
        this.idStage = idStage;
        this.idEtudiant = idEtudiant;
        this.idProfesseurVisite = idProfesseurVisite;
        this.idProfesseurSuivi = idProfesseurSuivi;
        this.classe = classe;
        this.nom = nom;
        this.prenom = prenom;
        this.professeurVisite = professeurVisite;
        this.professeurSuivi = professeurSuivi;
        this.promotion = promotion;
    }

    public int getIdStage() {
        return idStage;
    }

    public int getIdEtudiant() {
        return idEtudiant;
    }

    public int getIdProfesseurVisite() {
        return idProfesseurVisite;
    }

    public int getIdProfesseurSuivi() {
        return idProfesseurSuivi;
    }

    public String getClasse() {
        return classe;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getProfesseurVisite() {
        return professeurVisite;
    }

    public String getProfesseurSuivi() {
        return professeurSuivi;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setIdStage(int idStage) {
        this.idStage = idStage;
    }

    public void setIdEtudiant(int idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public void setIdProfesseurVisite(int idProfesseurVisite) {
        this.idProfesseurVisite = idProfesseurVisite;
    }

    public void setIdProfesseurSuivi(int idProfesseurSuivi) {
        this.idProfesseurSuivi = idProfesseurSuivi;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setProfesseurVisite(String professeurVisite) {
        this.professeurVisite = professeurVisite;
    }

    public void setProfesseurSuivi(String professeurSuivi) {
        this.professeurSuivi = professeurSuivi;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }
      
}
