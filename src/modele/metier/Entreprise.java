/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author Freazar
 */
public class Entreprise {
    private int idEntreprise;
    private String nom;
    private String adresse;
    private String ville;
    private int codePostal;
    private String secteurAct;

    public Entreprise(int idEntreprise, String nom, String adresse, String ville, int codePostal, String secteurAct) {
        this.idEntreprise = idEntreprise;
        this.nom = nom;
        this.adresse = adresse;
        this.ville = ville;
        this.codePostal = codePostal;
        this.secteurAct = secteurAct;
    }

    public int getIdEntreprise() {
        return idEntreprise;
    }

    public void setIdEntreprise(int idEntreprise) {
        this.idEntreprise = idEntreprise;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public String getSecteurAct() {
        return secteurAct;
    }

    public void setSecteurAct(String secteurAct) {
        this.secteurAct = secteurAct;
    }
    
    
}
