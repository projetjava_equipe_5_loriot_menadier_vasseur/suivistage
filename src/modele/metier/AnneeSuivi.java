/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author Freazar
 */
public class AnneeSuivi {
    private int idEtudiant;
    private String promotion;
    private int idClasse;

    public AnneeSuivi(int idEtudiant, String promotion, int idClasse) {
        this.idEtudiant = idEtudiant;
        this.promotion = promotion;
        this.idClasse = idClasse;
    }

    public int getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(int idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public int getIdClasse() {
        return idClasse;
    }

    public void setIdClasse(int idClasse) {
        this.idClasse = idClasse;
    }
    
    
}
