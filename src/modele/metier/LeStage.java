/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author melo_
 */
public class LeStage {
    private int idStage;
    private String classe;
    private String nom;
    private String prenom;
    private String professeurvisite;
    private String professeursuivi;
    private String datedeb;
    private String datefin;
    private String entreprise;
    private String adresse;
    private String maitredestage;

    public LeStage(int idStage, String classe, String nom, String prenom, String professeurvisite, String professeursuivi, String datedeb, String datefin, String entreprise, String adresse, String maitredestage) {
        this.idStage = idStage;
        this.classe = classe;
        this.nom = nom;
        this.prenom = prenom;
        this.professeurvisite = professeurvisite;
        this.professeursuivi = professeursuivi;
        this.datedeb = datedeb;
        this.datefin = datefin;
        this.entreprise = entreprise;
        this.adresse = adresse;
        this.maitredestage = maitredestage;
    }

    public int getIdStage() {
        return idStage;
    }

    public void setIdStage(int idStage) {
        this.idStage = idStage;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getProfesseurvisite() {
        return professeurvisite;
    }

    public void setProfesseurvisite(String professeurvisite) {
        this.professeurvisite = professeurvisite;
    }

    public String getProfesseursuivi() {
        return professeursuivi;
    }

    public void setProfesseursuivi(String professeursuivi) {
        this.professeursuivi = professeursuivi;
    }

    public String getDatedeb() {
        return datedeb;
    }

    public void setDatedeb(String datedeb) {
        this.datedeb = datedeb;
    }

    public String getDatefin() {
        return datefin;
    }

    public void setDatefin(String datefin) {
        this.datefin = datefin;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getMaitredestage() {
        return maitredestage;
    }

    public void setMaitredestage(String maitredestage) {
        this.maitredestage = maitredestage;
    }
    
    
    
}
