/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author Freazar
 */
public class MaitreStage {
    private int idMaitreStage;
    private int idEntreprise;
    private String nom;
    private String prenom;
    private String numtel;
    private String mail;
    private String fonction;

    public MaitreStage(int idMaitreStage, int idEntreprise, String nom, String prenom, String numtel, String mail, String fonction) {
        this.idMaitreStage = idMaitreStage;
        this.idEntreprise = idEntreprise;
        this.nom = nom;
        this.prenom = prenom;
        this.numtel = numtel;
        this.mail = mail;
        this.fonction = fonction;
    }

    public int getIdMaitreStage() {
        return idMaitreStage;
    }

    public void setIdMaitreStage(int idMaitreStage) {
        this.idMaitreStage = idMaitreStage;
    }

    public int getIdEntreprise() {
        return idEntreprise;
    }

    public void setIdEntreprise(int idEntreprise) {
        this.idEntreprise = idEntreprise;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNumtel() {
        return numtel;
    }

    public void setNumtel(String numtel) {
        this.numtel = numtel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }
}