/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author Freazar
 */
public class ProfesseurPrincipal {
    private int idClasse;
    private String promotion;
    private int idProfesseurPrincipal;

    public ProfesseurPrincipal(int idClasse, String promotion, int idProfesseurPrincipal) {
        this.idClasse = idClasse;
        this.promotion = promotion;
        this.idProfesseurPrincipal = idProfesseurPrincipal;
    }

    public int getIdClasse() {
        return idClasse;
    }

    public void setIdClasse(int idClasse) {
        this.idClasse = idClasse;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public int getIdProfesseurPrincipal() {
        return idProfesseurPrincipal;
    }

    public void setIdProfesseurPrincipal(int idProfesseurPrincipal) {
        this.idProfesseurPrincipal = idProfesseurPrincipal;
    }


    
}
