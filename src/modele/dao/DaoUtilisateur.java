/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.Utilisateur;

/**
 *
 * @author dami2
 */
public class DaoUtilisateur {
        /**
     * Extraction d'un stage, sur son identifiant
     * @param idUtilisateur identifiant
     * @return objet Utilisateur
     * @throws SQLException 
     */
    public static Utilisateur selectOne(int idUtilisateur) throws SQLException {
        Utilisateur unUtilisateur = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM UTILISATEUR WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idUtilisateur);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            String prenom = rs.getString("PRENOM");
            String nom = rs.getString("NOM");
            String login = rs.getString("LOGIN");
            String mdp = rs.getString("MPD");
            
            
            unUtilisateur = new Utilisateur(idUtilisateur, prenom, nom, login, mdp);
        }
        return unUtilisateur;
    }
    

    /**
     * Extraction de tous les stages
     * @return collection de stages
     * @throws SQLException 
     */
    public static List<Utilisateur> selectAll() throws SQLException {
        List<Utilisateur> lesUtilisateurs = new ArrayList<Utilisateur>();
        Utilisateur unUtilisateur;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM UTILISATEUR";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int idUtilisateur = rs.getInt("IDUTILISATEUR");
            String prenom = rs.getString("PRENOM");
            String nom = rs.getString("NOM");
            String login = rs.getString("LOGIN");
            String mdp = rs.getString("MPD");
            unUtilisateur = new Utilisateur(idUtilisateur, prenom, nom, login, mdp);
            lesUtilisateurs.add(unUtilisateur);
        }
        return lesUtilisateurs;
    }
}