/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.Entreprise;

/**
 *
 * @author Freazar
 */
public class DaoEntreprise {
     /**
     * Extraction d'une entreprise, sur son identifiant
     * @param idEntreprise identifiant
     * @return objet Entreprise
     * @throws SQLException 
     */
    public static Entreprise selectOne(int idEntreprise) throws SQLException {
        Entreprise uneEntreprise = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ENTREPRISE WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idEntreprise);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDENTREPRISE");
            String nom = rs.getString("NOM");
            String adresse = rs.getString("ADRESSE");
            String ville = rs.getString("VILLE");
            int cp = rs.getInt("CODEPOSTAL");
            String sect = rs.getString("SECTEURACT");
            
            uneEntreprise = new Entreprise(id, nom, adresse, ville, cp, sect);
        }
        return uneEntreprise;
    }
    

    /**
     * Extraction de toutes les entreprises
     * @return collection d'entreprises
     * @throws SQLException 
     */
    public static List<Entreprise> selectAll() throws SQLException {
        List<Entreprise> lesEntreprises = new ArrayList<Entreprise>();
        Entreprise uneEntreprise;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ENTREPRISE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("IDENTREPRISE");
            String nom = rs.getString("NOM");
            String adresse = rs.getString("ADRESSE");
            String ville = rs.getString("VILLE");
            int cp = rs.getInt("CODEPOSTAL");
            String sect = rs.getString("SECTEURACT");
            uneEntreprise = new Entreprise(id, nom, adresse, ville, cp, sect);
            lesEntreprises.add(uneEntreprise);
        }
        return lesEntreprises;
    }
}
