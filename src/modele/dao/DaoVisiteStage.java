/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.VisiteStage;

/**
 *
 * @author Freazar
 */
public class DaoVisiteStage {
    /**
     * Extraction d'une visite de stage, sur son identifiant
     * @param idVisiteStage identifiant
     * @return objet VisiteStage
     * @throws SQLException 
     */
    public static VisiteStage selectOne(int idVisiteStage) throws SQLException {
        VisiteStage uneVisiteStage = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM VISITESTAGE WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idVisiteStage);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDVISITESTAGE");
            int idStage = rs.getInt("IDSTAGE");
            int idUtilisateur = rs.getInt("IDPROFESSEUR");
            String dateVisite = rs.getString("DATEVISITE");
            boolean isVisited = rs.getBoolean("ISVISITED");
            
            uneVisiteStage = new VisiteStage(id, idStage, idUtilisateur, dateVisite, isVisited);
        }
        return uneVisiteStage;
    }
    

    /**
     * Extraction de toutes les visites de stages
     * @return collection de visite de stage
     * @throws SQLException 
     */
    public static List<VisiteStage> selectAll() throws SQLException {
        List<VisiteStage> lesVisitesStages = new ArrayList<VisiteStage>();
        VisiteStage uneVisiteStage;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM VISITESTAGE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("IDVISITESTAGE");
            int idStage = rs.getInt("IDSTAGE");
            int idUtilisateur = rs.getInt("IDPROFESSEUR");
            String dateVisite = rs.getString("DATEVISITE");
            boolean isVisited = rs.getBoolean("ISVISITED");
            
            uneVisiteStage = new VisiteStage(id, idStage, idUtilisateur, dateVisite, isVisited);
            lesVisitesStages.add(uneVisiteStage);
        }
        return lesVisitesStages;
    }
}
