/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.Etudiant;

/**
 *
 * @author Freazar
 */
public class DaoEtudiant {
    /**
     * Extraction d'un etudiant, sur son identifiant
     * @param idEtudiant identifiant
     * @return objet Etudiant
     * @throws SQLException 
     */
    public static Etudiant selectOne(int idEtudiant) throws SQLException {
        Etudiant unEtudiant = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ETUDIANT WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idEtudiant);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDETUDIANT");
            String specialite = rs.getString("SPECIALITE");
            String prenom = rs.getString("PRENOM");
            String nom = rs.getString("NOM");
            String login = rs.getString("LOGIN");
            String mdp = rs.getString("MDP");
            
            unEtudiant = new Etudiant(id, specialite, prenom, nom, login, mdp);
        }
        return unEtudiant;
    }
    

    /**
     * Extraction de tous les étudiants
     * @return collection d'étudiants
     * @throws SQLException 
     */
    public static List<Etudiant> selectAll() throws SQLException {
        List<Etudiant> lesEtudiants = new ArrayList<Etudiant>();
        Etudiant unEtudiant;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ETUDIANT";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("IDETUDIANT");
            String specialite = rs.getString("SPECIALITE");
            String prenom = rs.getString("PRENOM");
            String nom = rs.getString("NOM");
            String login = rs.getString("LOGIN");
            String mdp = rs.getString("MDP");
            
            unEtudiant = new Etudiant(id, specialite, prenom, nom, login, mdp);
            lesEtudiants.add(unEtudiant);
        }
        return lesEtudiants;
    }
}
