/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.MaitreStage;

/**
 *
 * @author Freazar
 */
public class DaoMaitreStage {
    /**
     * Extraction d'un maitre de stage, sur son identifiant
     * @param idMaitreStage identifiant
     * @return objet MaitreStage
     * @throws SQLException 
     */
    public static MaitreStage selectOne(int idMaitreStage) throws SQLException {
        MaitreStage unMaitreStage = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM MAITRESTAGE WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idMaitreStage);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDMAITRESTAGE");
            int idEntreprise = rs.getInt("IDENTREPRISE");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            String numTel = rs.getString("NUMTEL");
            String mail = rs.getString("MAIL");
            String fonction = rs.getString("FONCTION");
            
            unMaitreStage = new MaitreStage(id, idEntreprise, nom, prenom, numTel, mail, fonction);
        }
        return unMaitreStage;
    }
    

    /**
     * Extraction de tous les maitres de stages
     * @return collection de maitres de stages
     * @throws SQLException 
     */
    public static List<MaitreStage> selectAll() throws SQLException {
        List<MaitreStage> lesMaitresStages = new ArrayList<MaitreStage>();
        MaitreStage unMaitreStage;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM MAITRESTAGE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("IDMAITRESTAGE");
            int idEntreprise = rs.getInt("IDENTREPRISE");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            String numTel = rs.getString("NUMTEL");
            String mail = rs.getString("MAIL");
            String fonction = rs.getString("FONCTION");
            
            unMaitreStage = new MaitreStage(id, idEntreprise, nom, prenom, numTel, mail, fonction);
            lesMaitresStages.add(unMaitreStage);
        }
        return lesMaitresStages;
    }
}
