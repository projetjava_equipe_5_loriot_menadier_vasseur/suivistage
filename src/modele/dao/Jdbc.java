package modele.dao;

import java.sql.*;

/**
 * Singleton fournit un objet de connexion JDBC
 *
 * @author nbourgeois
 * @version 2 22 novembre 2013
 * @version 2019 : disparition de l'attribut piloteJdbc (obsolète)
 * àversion 2020 : disparition de l'attribut protocolesJdbc, intégré à serveurBd (simplification
 */
public class Jdbc {

    // Instance du singleton Jdbc
    private static Jdbc singleton = null;
    // Paramètre de la connexion local
    /*
    private String serveurBd = "jdbc:mysql://localhost/SUIVISATGE";
    private String nomBd = "SUIVISTAGE";
    private String loginSgbd = "root";
    private String mdpSgbd = "";
    */
    //Paramètre de la connexion distante
    private String serveurBd = "jdbc:mysql://91.160.18.96:3306/";
    private String nomBd = "mmenadier_BDD_suivistage";
    private String loginSgbd = "mmenadier";
    private String mdpSgbd = "164e90";
    
    // Connexion
    private Connection connexion = null; // java.sql.Connection

    private Jdbc() {
        //this.serveurBd = "jdbc:mysql://91.160.18.96:3306/mmenadier_BDD_suivistage";
        //this.nomBd = "mmenadier_BDD_suivistage";
        //this.loginSgbd = "mmenadier";
        //this.mdpSgbd = "164e90";
    }

    /**
     * @param serveur : adresse du serveur + port (fini par un /, sauf pour
     * Oracle ; BD pour embarquée : chemin accès répertoire )
     * @param base : nom de la BD ou du DSN pour ODBC
     * @param login : utilisateur autorisé du SGBD (ou schéma Oracle)
     * @param mdp : son mot de passe
     */
    private Jdbc(String serveur, String base, String login, String mdp) {
        this.serveurBd = serveur;
        this.nomBd = base;
        this.loginSgbd = login;
        this.mdpSgbd = mdp;
    }

    public static Jdbc creer(String serveur, String base, String login, String mdp) {
        if (singleton == null) {
            singleton = new Jdbc(serveur, base, login, mdp);
        }
        return singleton;
    }
    
    public static Jdbc creerDefault() {
        if (singleton == null) {
            singleton = new Jdbc();
        }
        return singleton;
    }

    public static Jdbc getInstance() {
        return singleton;
    }

    public void connecter() throws SQLException {
        connexion = DriverManager.getConnection(serveurBd + nomBd, loginSgbd, mdpSgbd);
        connexion.setAutoCommit(true);
    }

    public void deconnecter() throws SQLException {
        connexion.close();
    }

    public static java.sql.Date utilDateToSqlDate(java.util.Date uneDate) {
        return (new java.sql.Date(uneDate.getTime()));
    }

    /*
     * *************************************
     * ACCESSEURS 
     * **************************************
     */

    public String getServeurBd() {
        return serveurBd;
    }

    public void setServeurBd(String serveurBd) {
        this.serveurBd = serveurBd;
    }

    public String getNomBd() {
        return nomBd;
    }

    public void setNomBd(String nomBd) {
        this.nomBd = nomBd;
    }

    public String getLoginSgbd() {
        return loginSgbd;
    }

    public void setLoginSgbd(String loginSgbd) {
        this.loginSgbd = loginSgbd;
    }

    public String getMdpSgbd() {
        return mdpSgbd;
    }

    public void setMdpSgbd(String mdpSgbd) {
        this.mdpSgbd = mdpSgbd;
    }

    public Connection getConnexion() {
        return connexion;
    }

    public void setConnexion(Connection connexion) {
        this.connexion = connexion;
    }
}