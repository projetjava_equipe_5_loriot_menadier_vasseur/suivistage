/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.Professeur;

/**
 *
 * @author Freazar
 */
public class DaoProfesseur {
    /**
     * Extraction d'un professeur, sur son identifiant
     * @param idProfesseur identifiant
     * @return objet Professeur
     * @throws SQLException 
     */
    public static Professeur selectOne(int idProfesseur) throws SQLException {
        Professeur unProfesseur = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM PROFESSEUR WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idProfesseur);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDPROFESSEUR");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            String login = rs.getString("LOGIN");
            String mdp = rs.getString("MDP");
            
            unProfesseur = new Professeur(id, nom, prenom, login, mdp);
        }
        return unProfesseur;
    }
    

    /**
     * Extraction de tous les professeurs
     * @return collection de professeurs
     * @throws SQLException 
     */
    public static List<Professeur> selectAll() throws SQLException {
        List<Professeur> lesProfesseurs = new ArrayList<Professeur>();
        Professeur unProfesseur;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM PROFESSEUR";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("IDPROFESSEUR");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            String login = rs.getString("LOGIN");
            String mdp = rs.getString("MDP");
            
            unProfesseur = new Professeur(id, nom, prenom, login, mdp);
            lesProfesseurs.add(unProfesseur);
        }
        return lesProfesseurs;
    }
}
