/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.LeStage;

/**
 *
 * @author melo_
 */
public class DaoLeStage {
     /**
     * Extraction d'un etudiant, sur son identifiant
     * @param idStage identifiant
     * @return objet Etudiant
     * @throws SQLException 
     */
    public static LeStage selectOneById(int idStage) throws SQLException {
        LeStage unStage = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM LESTAGE WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idStage);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDSTAGE");
            String classe = rs.getString("CLASSE");
            String nom = rs.getString("NOMETUDIANT");
            String prenom = rs.getString("PRENOMETUDIANT");
            String professeurvisite = rs.getString("PROFESSEURVISITE");
            String professeursuivi = rs.getString("PROFESSEURSUIVI");
            String datedeb = rs.getString("DATEDEB");
            String datefin = rs.getString("DATEFIN");
            String entreprise = rs.getString("ENTREPRISE");
            String adresse = rs.getString("ADRESSE");
            String maitredestage = rs.getString("MATREDESTAGE");
            
            unStage = new LeStage(idStage, classe, nom, prenom, professeurvisite, professeursuivi, datedeb, datefin, entreprise, adresse, maitredestage);
        }
        return unStage;
    }
}

