/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.Classe;

/**
 *
 * @author Freazar
 */
public class DaoClasse {
    /**
     * Extraction d'une classe, sur son identifiant
     * @param idClasse identifiant
     * @return objet Classe
     * @throws SQLException 
     */
    public static Classe selectOne(int idClasse) throws SQLException {
        Classe uneClasse = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM CLASSE WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idClasse);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDCLASSE");
            String libelle = rs.getString("LIBELLECLASSE");
            
            uneClasse = new Classe(id, libelle);
        }
        return uneClasse;
    }
    

    /**
     * Extraction de toutes les classes
     * @return collection de classes
     * @throws SQLException 
     */
    public static List<Classe> selectAll() throws SQLException {
        List<Classe> lesClasses = new ArrayList<Classe>();
        Classe uneClasse;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM CLASSE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("IDCLASSE");
            String libelle = rs.getString("LIBELLECLASSE");

            uneClasse = new Classe(id, libelle);
            lesClasses.add(uneClasse);
        }
        return lesClasses;
    }
}
