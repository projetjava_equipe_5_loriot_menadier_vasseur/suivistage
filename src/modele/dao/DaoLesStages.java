/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.LesStages;

/**
 *
 * @author dami2
 */
public class DaoLesStages {
    
    /**
     * Extraction d'un stage, sur son identifiant
     * @param idStage identifiant
     * @return objet Stage
     * @throws SQLException 
     */
    /*
    public static LesStages selectAllByIdEtudiant(int idStage) throws SQLException {
        LesStages ListeLesStages = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        //TOEND
        String requete = "SELECT * FROM LESSTAGES WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idStage);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDSTAGE");
            int nom = rs.getInt("NOM");
            int prenom = rs.getInt("PRENOM");
            int professeurVisite = rs.getInt("PROFESSEURVISITE");
            String professeurSuivi = rs.getString("PROFESSEURSUIVI");
            String dateDebut = rs.getString("DATEDEBUT");
            String dateFin = rs.getString("DATEFIN");
            boolean isDone = rs.getBoolean("ISDONE");
            
            ListeLesStages = new Stage(id, idEntreprise, idMaitreStage, idUtilisateur, dateDebut, dateFin, isDone);
        }
        return unStage;
    }*/
    

    /**
     * Extraction de tous les stages
     * @return collection de stages
     * @throws SQLException 
     */
    public static List<LesStages> selectAll() throws SQLException {
        List<LesStages> listeLesStages = new ArrayList<LesStages>();
        LesStages unStage;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM LESSTAGES";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            //On récupère les ID qui permettront de mettre en place les filtres
            int idStage = rs.getInt("IDSTAGE");
            int idEtudiant = rs.getInt("IDETUDIANT");
            int idProfesseurVisite = rs.getInt("IDPROFESSEURVISITE");
            //Le prof principal est le professeur chargé du suivi
            int idProfesseurSuivi = rs.getInt("IDPROFESSEURSUIVI");
            
            //On récupères les informations nécessaires à l'affichage pour la vue
            String classe = rs.getString("CLASSE");
            String nomEtudiant = rs.getString("NOMETUDIANT");
            String prenomEtudiant = rs.getString("PRENOMETUDIANT");
            String nomProfesseurVisite = rs.getString("PROFESSEURVISITE");
            //Il est aussi le professeur principal
            String nomProfesseurSuivi = rs.getString("PROFESSEURSUIVI");
            String promotion = rs.getString("PROMOTION");
            unStage = new LesStages(idStage, idEtudiant, idProfesseurVisite, idProfesseurSuivi,
                    classe, nomEtudiant, prenomEtudiant, nomProfesseurVisite, nomProfesseurSuivi, promotion);
            listeLesStages.add(unStage);
        }
        return listeLesStages;
    }
    
    /**
     * Extraction de tous les stages qui ont un professeurs chargé de visite
     * @return collection de String
     * @throws SQLException 
     */
    public static List<String> selectLesProfesseursVisite() throws SQLException {
        List<String> listeDesProfesseursVisite = new ArrayList<String>();
        String unProfesseur;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT DISTINCT(PROFESSEURVISITE) FROM LESSTAGES";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            //On récupère les ID qui permettront de mettre en place les filtres
            String nomProfesseurVisite = rs.getString("PROFESSEURVISITE");

            unProfesseur = nomProfesseurVisite;
            
            listeDesProfesseursVisite.add(unProfesseur);
        }
        return listeDesProfesseursVisite;
    }
    
    /**
     * Extraction de tous les stages qui ont un professeurs chargé d'un suivi
     * @return collection de String
     * @throws SQLException 
     */
    public static List<String> selectLesProfesseursSuivi() throws SQLException {
        List<String> listeDesProfesseursSuivi = new ArrayList<String>();
        String unProfesseur;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT DISTINCT(PROFESSEURSUIVI) FROM LESSTAGES";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            //On récupère les ID qui permettront de mettre en place les filtres
            String nomProfesseurSuivi = rs.getString("PROFESSEURSUIVI");

            unProfesseur = nomProfesseurSuivi;
            
            listeDesProfesseursSuivi.add(unProfesseur);
        }
        return listeDesProfesseursSuivi;
    }
    
    /**
     * Extraction de tous les stages d'un étudiant en fonction de son nom
     * @return collection de stages
     * @throws SQLException 
     */
    public static List<LesStages> selectStagesNomEtudiant(String nom) throws SQLException {
        List<LesStages> listeLesStages = new ArrayList<LesStages>();
        LesStages unStage;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM LESSTAGES WHERE NOMETUDIANT= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setString(1, nom);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            //On récupère les ID qui permettront de mettre en place les filtres
            int idStage = rs.getInt("IDSTAGE");
            int idEtudiant = rs.getInt("IDETUDIANT");
            int idProfesseurVisite = rs.getInt("IDPROFESSEURVISITE");
            //Le prof principal est le professeur chargé du suivi
            int idProfesseurSuivi = rs.getInt("IDPROFESSEURSUIVI");
            
            //On récupères les informations nécessaires à l'affichage pour la vue
            String classe = rs.getString("CLASSE");
            String nomEtudiant = rs.getString("NOMETUDIANT");
            String prenomEtudiant = rs.getString("PRENOMETUDIANT");
            String nomProfesseurVisite = rs.getString("PROFESSEURVISITE");
            //Il est aussi le professeur principal
            String nomProfesseurSuivi = rs.getString("PROFESSEURSUIVI");
            String promotion = rs.getString("PROMOTION");
            unStage = new LesStages(idStage, idEtudiant, idProfesseurVisite, idProfesseurSuivi,
                    classe, nomEtudiant, prenomEtudiant, nomProfesseurVisite, nomProfesseurSuivi, promotion);
            listeLesStages.add(unStage);
        }
        return listeLesStages;
    }
}
