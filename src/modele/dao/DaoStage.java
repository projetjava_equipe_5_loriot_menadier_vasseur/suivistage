
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.Stage;

/**
 *
 * @author Freazar
 */
public class DaoStage {
     /**
     * Extraction d'un stage, sur son identifiant
     * @param idStage identifiant
     * @return objet Stage
     * @throws SQLException 
     */
    public static Stage selectOne(int idStage) throws SQLException {
        Stage unStage = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM STAGE WHERE IDSTAGE= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idStage);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDSTAGE");
            int idEntreprise = rs.getInt("IDENTREPRISE");
            int idMaitreStage = rs.getInt("IDMAITRESTAGE");
            int idUtilisateur = rs.getInt("IDETUDIANT");
            String dateDebut = rs.getString("DATEDEBUT");
            String dateFin = rs.getString("DATEFIN");
            boolean isDone = rs.getBoolean("ISDONE");
            
            unStage = new Stage(id, idEntreprise, idMaitreStage, idUtilisateur, dateDebut, dateFin, isDone);
        }
        return unStage;
    }
    

    /**
     * Extraction de tous les stages
     * @return collection de stages
     * @throws SQLException 
     */
    public static List<Stage> selectAll() throws SQLException {
        List<Stage> lesStages = new ArrayList<Stage>();
        Stage unStage;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM STAGE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("IDSTAGE");
            int idEntreprise = rs.getInt("IDENTREPRISE");
            int idMaitreStage = rs.getInt("IDMAITRESTAGE");
            int idUtilisateur = rs.getInt("IDETUDIANT");
            String dateDebut = rs.getString("DATEDEBUT");
            String dateFin = rs.getString("DATEFIN");
            boolean isDone = rs.getBoolean("ISDONE");
            unStage = new Stage(id, idEntreprise, idMaitreStage, idUtilisateur, dateDebut, dateFin, isDone);
            lesStages.add(unStage);
        }
        return lesStages;
    }

}